<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    @extends("layout")
    @section("form")
    <section class="input-form">
        <h1>Todo list</h1>
        <div class="input">
            <label for="text">New todo item</label>
            <input type="text" id="text">
        </div>
        <div>
            <button>Save item</button>
        </div>
    </section>


    @endsection
</body>
</html>